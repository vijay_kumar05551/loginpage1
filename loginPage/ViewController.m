//
//  ViewController.m
//  loginPage
//
//  Created by Cli16 on 11/17/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
NSString *userName;
NSString *password;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITextField *userTextField;
@property (strong, nonatomic) IBOutlet UITextField *passTextField;
@property (strong, nonatomic) IBOutlet UIButton *getInfoBtn;
@property (strong, nonatomic) IBOutlet UIButton *getUserNameBtn;

@end

@implementation ViewController
@synthesize userTextField;
@synthesize passTextField;
@synthesize getInfoBtn;
@synthesize getUserNameBtn;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    passTextField.secureTextEntry=YES;
    

}

- (IBAction)getInfoBtnPressed:(id)sender {
    
    userName=userTextField.text;
    password=passTextField.text;
    [[NSUserDefaults standardUserDefaults]setObject:userName forKey:@"userName"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults]setObject:password forKey:@"passward"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSLog(@"your username is= %@",userName);
    
    NSLog(@"your password is= %@",password);
    
    
}
- (IBAction)getUserNameBtnPressed:(id)sender {
    NSString *userName=[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
    UIAlertView *userNameAlert = [[UIAlertView alloc] initWithTitle:@"Your username is"
                                                            message:userName
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
    [userNameAlert show];
    

}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
     if ([segue.identifier isEqualToString:@"goTo1"]) {
      [[NSUserDefaults standardUserDefaults]setObject:password forKey:@"passward"];
         [[NSUserDefaults standardUserDefaults]synchronize];
     }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
