//
//  ViewController2.m
//  loginPage
//
//  Created by Cli16 on 11/17/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController2.h"

@interface ViewController2 ()
@property (strong, nonatomic) IBOutlet UIButton *nextBtn;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;

@end

@implementation ViewController2
@synthesize nextBtn;
@synthesize userNameLabel;
@synthesize passwordLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *doubleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapPressed:)];
    doubleTap.numberOfTapsRequired=2;
    doubleTap.numberOfTouchesRequired=1;
    //  [doubleTap addTarget:self action:@selector(doubleTapPressed:)];
    [self.view addGestureRecognizer:doubleTap];

    // Do any additional setup after loading the view.
}
-(void)doubleTapPressed:(UITapGestureRecognizer *)sender
{
    NSString *userName=[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
    userNameLabel.text=[NSString stringWithFormat:@"%@",userName];
    [[NSUserDefaults standardUserDefaults]synchronize];
   
    NSString *password=[[NSUserDefaults standardUserDefaults]objectForKey:@"passward"];
    passwordLabel.text=[NSString stringWithFormat:@"%@",password];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
- (IBAction)nextButtonPressed:(id)sender {
    
    NSString *userName=[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
    userNameLabel.text=[NSString stringWithFormat:@"%@",userName];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString *password=[[NSUserDefaults standardUserDefaults]objectForKey:@"passward"];
    passwordLabel.text=[NSString stringWithFormat:@"%@",password];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    }


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
